import { configureStore} from "@reduxjs/toolkit";
import productsReducer from "../Components/Slices/productsSlice";
import modalSlice from "../Components/Slices/modalSlice";


const store = configureStore({
    reducer: {
        products: productsReducer,
        modal: modalSlice
    }
})

export default store;