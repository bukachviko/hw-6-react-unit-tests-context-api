import React, {useContext} from 'react';
import {FaList} from "react-icons/fa";
import {MdGridView} from "react-icons/md";
import "./SwitchViewBtn.css";
import ProductContext from "../Context/ProductContext";

const SwitchViewBtn = () => {

    const {handleProductView} = useContext(ProductContext);

    return (
        <div className = "switch-btn">
            <button className="switch-btn-list"
                    onClick={() => {
                        handleProductView(false)
                    }}
                        >
                <FaList />
            </button>
            <button className="switch-btn-grid"
                    onClick={() => {
                        handleProductView(true)
                    }}
                   >
                <MdGridView />
            </button>
        </div>
    );
};

export default SwitchViewBtn;