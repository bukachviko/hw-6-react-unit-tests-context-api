import React from "react";

const Button = ({btnClass, backgroundColor, text, myClick, dataTestId}) => {
    return (
        <button
            className={btnClass}
            onClick= {myClick}
            style={{backgroundColor}}
            data-testid={dataTestId}
        >
            {text}
        </button>
    )
}

export default Button;