import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

export const fetchCardData = createAsyncThunk(
    'products/fetchCardData',
    async () => {
        try {
            const response = await fetch("http://localhost:3001/posts");

            return await response.json();
        } catch (error) {
            throw Error('Помилка отримання даних');
        }
    });

const productsSlice = createSlice({
    name: 'products',
    initialState: {
        data: [],
        status: 'idle',
        error: null,
        currentProductIdToCart: 0,
        currentProductIdDeleteCart: 0,
        cart: [],
        selected: [],
    },
    reducers: {
        checkCurrentProductIdToCart: (state, action) => {
            state.currentProductIdToCart = action.payload;
        },

        initCart: (state) => {
            let prodFromLocalSt = JSON.parse(localStorage.getItem('cartItems'))
            if (Array.isArray(prodFromLocalSt)) {
                state.cart = prodFromLocalSt;
            }
        },

        clearProductToCard: (state) => {
            localStorage.removeItem('cartItems');
            state.cart = [];
        },

        addToCart: (state, action) => {
            state.cart.push(action.payload);
            localStorage.setItem('cartItems', JSON.stringify(state.cart));
        },

        checkCurrentProductIdDeleteFromCart: (state, action) => {
            state.currentProductIdDeleteCart = action.payload;

        },

        deleteFromCart: (state, action) => {
            state.cart = state.cart.filter(item => item.id !== action.payload)
            localStorage.setItem('cartItems', JSON.stringify(state.cart));
        },

        toggleSelectedProducts: (state, action) => {
            if ((state.selected.filter(item => item.id === action.payload.id)).length) {
                state.selected = state.selected.filter(item => item.id !== action.payload.id)
            } else {
                state.selected.push(action.payload)
            }
        },

        clearCartFromLocalStorage: (state) => {
            localStorage.removeItem('cartItems');
            state.cart = [];
        },
    },

    extraReducers:
        (builder) => {
            builder.addCase(fetchCardData.pending, (state, action) => {
                state.status = 'loading'
            }).addCase(fetchCardData.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = action.payload;
                state.error = null
            }).addCase(fetchCardData.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message
            })
        }
})

export default productsSlice.reducer;
export const {
    initCart,
    checkCurrentProductIdToCart,
    addToCart,
    checkCurrentProductIdDeleteFromCart,
    deleteFromCart,
    toggleSelectedProducts,
    clearCartFromLocalStorage,
    clearProductToCard

} = productsSlice.actions;
