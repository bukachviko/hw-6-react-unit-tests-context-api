import Button from "../Button/Button";
import "./List.css";
import {HiOutlineStar, HiStar} from "react-icons/hi2";
import {useDispatch, useSelector} from "react-redux";
import {checkCurrentProductIdToCart, toggleSelectedProducts} from "../Slices/productsSlice";
import {openModal} from "../Slices/modalSlice";
import React from "react";

function ListProductCard(props) {
    const dispatch = useDispatch();
    const selected = useSelector(state => state.products.selected);

    return (
        <>
            <div className="card-item-contents">
                <div className="card-item__image-product">
                    <img src={props.image} alt=""/>
                </div>

                <div className="card-item-description-left">
                    <h3 className="card-item__title-product">{props.name}</h3>
                    <p className="card-item__color-product">{props.color}</p>
                    <p className="card-item__article-product">{props.article}</p>
                </div>

                    <div className="card-item-description-right">
                        <p className="card-item__price-product"> ${props.cost}</p>
                        <Button
                            key={1}
                            btnClass="item-btn__add-to-cart"
                            backgroundColor={'#833ab4'}
                            text={"Add to cart"}

                            myClick={
                                () => {
                                    dispatch(openModal())
                                    dispatch(checkCurrentProductIdToCart(props.productId))
                                }
                            }
                        />
                        <div className="card-item-actions__selected"
                             onClick={() => {
                                 dispatch(toggleSelectedProducts({
                                     id: props.productId,
                                     name: props.name,
                                     color: props.color,
                                     cost: props.cost,
                                     image: props.image
                                 }))
                             }
                             }>

                            {
                                (selected.filter(item => item.id === props.productId)).length
                                    ? <HiStar fill='rgba(131,58,180,1)'/>
                                    : <HiOutlineStar/>
                            }
                        </div>
                    </div>
            </div>
        </>
    );
}

export default ListProductCard;