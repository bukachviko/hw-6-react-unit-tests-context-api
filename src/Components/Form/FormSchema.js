import * as yup from 'yup';

export const formSchema = yup.object().shape({
    userFirstName: yup.string().required("Введіть своє ім`я").matches(/^[A-Za-z]{3,10}$/g, "до 10 символів"),
    userLastName: yup.string().required("Введіть своє прізвище").matches(/^[A-Za-z]{3,15}$/g, "до 15 символів"),
    userAge: yup.number().min(13).max(105, 'Недопустиме значення').positive( ).required("Введіть свій вік" ).integer(),
    userDeliveryAddress: yup.string().min(10).required("Введіть адресу доставки"),
    userPhone: yup.string()
})