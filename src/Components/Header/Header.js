import React from 'react';
import Cart from "../Cart/Cart";
import Selected from "../Selected/Selected";
import "./Header.css";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

function Header() {
    const cart = useSelector((state) => state.products.cart)
    const selected = useSelector(state => state.products.selected)

    return (
        <div className="header">
            <nav className="navbar">
                <h1>
                    <Link to="/" className="navbar__title">
                        Organic fruit store
                    </Link>
                </h1>
                <div className="header-links">
                    <ul>
                        <li>
                            <Link to="/selected">
                                <Selected countSelectedItems={selected.length}/>
                            </Link>
                        </li>
                        <li>
                            <Link to="/cart">
                                <Cart countCartItems={cart.length}/>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
}

export default Header;
