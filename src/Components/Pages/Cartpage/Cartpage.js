import React from 'react';
import Button from "../../Button/Button";
import "./Cartpage.css";
import {useDispatch, useSelector} from "react-redux";
import {checkCurrentProductIdDeleteFromCart, deleteFromCart} from "../../Slices/productsSlice";
import Modal from "../../Modal/Modal";
import {closeModal, openModal} from "../../Slices/modalSlice";
import FormGetInfo from "../../Form/FormGetInfo";

const Cartpage = () => {
    const cart = useSelector(state => state.products.cart);
    const dispatch = useDispatch();
    const isShowModal = useSelector(state => state.modal.isShowModal);
    const currentProductDeleteCart = useSelector(state => state.products.currentProductIdDeleteCart)

    const productDeleteFromCart = () => {
        dispatch(deleteFromCart(currentProductDeleteCart))
        dispatch(closeModal())
    }

    return (
        <>
            <div className="cart-items">
                <h2 className="cart-items-header">Товари додані у корзину:</h2>

                {cart.length === 0 && (
                    <div className="cart-items-empty">Ви ще не додали жодного товара у корзину</div>
                )}

                <div>
                    {cart.map((item, key) => (
                        <div key={key} className="cart-item">
                            <Button
                                btnClass="cart-items-delete-btn"
                                text={'Х'}
                                myClick={() => {
                                    dispatch(openModal())
                                    dispatch(checkCurrentProductIdDeleteFromCart(item.id))
                                }}
                            />
                            <img className="cart-items-img" src={item.image} alt=""/>
                            <h3 className="cart-items-name">{item.name}</h3>
                            <p className="cart-items-cost"> ${item.cost}</p>
                        </div>
                    ))}
                </div>
            </div>

            {isShowModal && <Modal
                classNameBtn={"del__item-cart-bnt"}
                backgroundColor={{backgroundColor: '#833ab4'}}
                header={"Видалити товар з кошика?"}
                text={"Підтвердіть видалення товару з кошика"}
                boolean={true}
                nameBtn1={'Так!'}
                nameBtn2={'Ні!'}
                functionSuccess={productDeleteFromCart}
            />}
            {cart.length > 0 && <FormGetInfo/>}
        </>
    );
};

export {Cartpage};
