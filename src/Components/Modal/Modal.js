import Button from "../Button/Button";
import "./Modal.css";
import {useDispatch} from "react-redux";
import {closeModal} from "../Slices/modalSlice";

export default function Modal({functionSuccess, backgroundColor, header, text: mainText, nameBtn1, nameBtn2, classNameBtn}) {
const dispatch = useDispatch();
    return (

        <div className="modal">
            <div data-testid="overlay"  onClick= {()=> dispatch(closeModal())}
                 className="overlay"></div>
            <div className="modal-content" style={backgroundColor}>
                <div className="header__modal">
                    <h1 className="header__modal-title">{header}</h1>
                    <Button data-testid="close-btn"
                        btnClass="modal__headerCloseBtn"
                        myClick= {()=> dispatch(closeModal())}
                        text={'Х'}
                    />
                </div>
                <p className="modal__text">{mainText}</p>
                <div className="modal__footer">
                    <Button
                        dataTestId="close-btn"
                        btnClass={classNameBtn}
                        text={nameBtn1}
                        myClick={functionSuccess}
                    />
                    <Button
                        text={nameBtn2}
                        myClick= {()=> dispatch(closeModal())}
                    />
                </div>
            </div>
        </div>
    );
}

