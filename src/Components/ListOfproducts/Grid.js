import React, {useContext} from 'react';
import GridProductCard from "../ProductCard/GridProductCard";
import ProductContext from "../Context/ProductContext";

const Grid = ({ data, status, error, cart }) => {
    const productContext = useContext(ProductContext);

    return (

            <ul className="card__container-grid">
                {status === 'loading' && <div>Loading...</div>}
                {status === 'failed' && <div>Error: {error}</div>}
                {status === 'succeeded' && data.length > 0 ? (
                    data.map((item, index) => {

                            let existEl = cart.find((element) => {
                                return element.id === item.id
                            });

                            let classes = 'card';

                            if (typeof existEl === "object") {
                                classes += " added-in-cart";
                            }

                            return <li className={classes} key={index}>
                                <GridProductCard
                                    image={item.image}
                                    name={item.name}
                                    color={item.color}
                                    cost={item.cost}
                                    article={item.article}
                                    productId={item.id}
                                />
                            </li>
                        }
                    )
                ) : (
                    <div>No data available</div>
                )}
            </ul>
    );
};

export default Grid;