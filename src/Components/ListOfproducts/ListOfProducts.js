import {useDispatch, useSelector} from 'react-redux';
import React, {useContext, useEffect} from "react";
import {addToCart, fetchCardData} from '../Slices/productsSlice';
import Modal from "../Modal/Modal";
import {closeModal} from "../Slices/modalSlice";
import Grid from "../ListOfproducts/Grid"
import List from "../ListOfproducts/List"
import SwitchViewBtn from "../Button/SwitchViewBtn";
import ProductContext from "../Context/ProductContext";

const ListOfProducts = () => {
    const {handleProductView, value} = useContext(ProductContext);
    console.log(value)

    const dispatch = useDispatch();
    const isShowModal = useSelector(state => state.modal.isShowModal);
    const data = useSelector(state => state.products.data);
    const status = useSelector((state) => state.products.status);
    const error = useSelector((state) => state.products.error);
    const cart = useSelector((state) => state.products.cart)
    const currentProductToCart = useSelector((state) => state.products.currentProductIdToCart)

    const productAddToCard = () => {
        if (!(cart.filter((item) => item.id === currentProductToCart)).length) {
            dispatch(addToCart(data.filter((item) => item.id === currentProductToCart)[0]))
        }

        dispatch((closeModal()))
    }

    useEffect(() => {
        dispatch(fetchCardData());

    }, [dispatch]);


    return (
        <>
            <SwitchViewBtn/>
            {value === true ? <Grid
                data={data}
                error={error}
                status={status}
                cart={cart}
            /> : <List
                data={data}
                error={error}
                status={status}
                cart={cart}
            />}


            {isShowModal && <Modal
                classNameBtn={"add__product-bnt"}
                backgroundColor={{backgroundColor: '#833ab4'}}
                header={"Додати товар в кошик?"}
                text={"Можливе замовлення від 1 шт"}
                boolean={true}
                nameBtn1={'Добре!'}
                nameBtn2={'Ні!'}
                functionSuccess={productAddToCard}
            />}
        </>
    );
}

export default ListOfProducts;
