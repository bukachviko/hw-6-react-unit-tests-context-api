import React, {useContext} from 'react';
import ListProductCard from "../ProductCard/ListProductCard";
import ProductContext from "../Context/ProductContext";


const List = ({ data, status, error, cart }) => {

    const productContext = useContext(ProductContext);

    return (
        <ul className="card__container-list">
            {status === 'loading' && <div>Loading...</div>}
            {status === 'failed' && <div>Error: {error}</div>}
            {status === 'succeeded' && data.length > 0 ? (
                data.map((item, index) => {

                        let existEl = cart.find((element) => {
                            return element.id === item.id
                        });

                        let classes = 'card-item';

                        if (typeof existEl === "object") {
                            classes += " added-in-cart";
                        }

                        return <li className={classes} key={index}>
                            <ListProductCard
                                image={item.image}
                                name={item.name}
                                color={item.color}
                                cost={item.cost}
                                article={item.article}
                                productId={item.id}
                            />
                        </li>
                    }
                )
            ) : (
                <div>No data available</div>
            )}
        </ul>
    );
};

export default List;