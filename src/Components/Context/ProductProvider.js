import React, {useState} from 'react';
import ProductContext from "./ProductContext";

const ProductProvider = ({children}) => {
    const [isProductGrid, setIsProductGrid] = useState(true);

    const handleProductView = (value) => {
        setIsProductGrid(value)
    }

    const productContextValue = {
        value: isProductGrid,
        handleProductView
    }

    return (
        <ProductContext.Provider value={productContextValue}>
            {children}
        </ProductContext.Provider>
    );
};

export {ProductProvider};