import React from 'react';
import '@testing-library/jest-dom'
import Modal from "../Components/Modal/Modal";
import {fireEvent, render} from "@testing-library/react";
import {Provider} from 'react-redux';
import store from "../store/store";

describe('Modal Component', () => {
    test('renders Modal component correctly', () => {
        const { getByText } = render(
            <Provider store={store}>
                <Modal
                    functionSuccess={() => {}}
                    backgroundColor={{ backgroundColor: 'white' }}
                    header="Test Header"
                    text="Test Text"
                    nameBtn1="Button 1"
                    nameBtn2="Button 2"
                    classNameBtn="button-class"
                />
            </Provider>
        );
        expect(getByText('Test Header')).toBeInTheDocument();
        expect(getByText('Test Text')).toBeInTheDocument();
        expect(getByText('Button 1')).toBeInTheDocument();
        expect(getByText('Button 2')).toBeInTheDocument();
    });

    test('closes Modal on overlay click', () => {
        const { getByTestId } = render(
            <Provider store={store}>
                <Modal functionSuccess={() => {}} />
            </Provider>
        );

        fireEvent.click(getByTestId('overlay'));
        expect(store.getState().modal.isShowModal).toBe(false);
    });

    test('close modal on close button click', () => {
        const { getByTestId } = render(
            <Provider store={store}>
                <Modal functionSuccess={() => {}} />
            </Provider>
        );
        // console.log(document.body.innerHTML);

        fireEvent.click(getByTestId('close-btn'));
        expect(store.getState().modal.isShowModal).toBe(false);
    });

})