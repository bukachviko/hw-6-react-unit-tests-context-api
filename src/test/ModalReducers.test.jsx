import '@testing-library/jest-dom'
import modalReducer, {closeModal, openModal} from "../Components/Slices/modalSlice";


describe('Modal Reducer', () => {
    test('should return the initial state', () => {
        const initialState = { isShowModal: false };
        const state = modalReducer(undefined, {});
        expect(state).toEqual(initialState);
    });

    test('should handle openModal', () => {
        const initialState = { isShowModal: false };
        const state = modalReducer(initialState, openModal());
        expect(state.isShowModal).toBe(true);
    });

    test('should handle closeModal', () => {
        const initialState = { isShowModal: true };
        const state = modalReducer(initialState, closeModal());
        expect(state.isShowModal).toBe(false);
    });

    test('should return the current state for unknown action', () => {
        const currentState = { isShowModal: true };
        const state = modalReducer(currentState, { type: 'UNKNOWN-ACTION'});
        expect(state).toEqual(currentState);
    });


})