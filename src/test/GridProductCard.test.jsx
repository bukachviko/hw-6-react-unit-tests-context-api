import React from 'react';
import {fireEvent, render} from '@testing-library/react'
import renderer from "react-test-renderer";
import {Provider} from 'react-redux';
import store from "../store/store";
import GridProductCard from "../Components/ProductCard/GridProductCard";

describe('GridProductCard Component', () => {
    test('renders correctly with test data', () => {
        const tree = renderer
            .create(
                <Provider store={store}>
                    <GridProductCard
                    image="Test-image.jpg"
                    name="Test-NameProduct"
                    color="White"
                    cost={2}
                    article="123456"
                    productId={2}
                    />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('triggers modal open and product check on "Add to cart" button click', () => {
        const { getByText } = render(
            <Provider store={store}>
                <GridProductCard
                    image="Test-image.jpg"
                    name="Test-NameProduct"
                    color="Blu"
                    cost={3}
                    article="123453"
                    productId={3}
                />
            </Provider>
        );
        fireEvent.click(getByText('Add to cart'));
    });

    test('toggles product selection on actions__selected click', () => {
        const { getByTestId } = render (
            <Provider store={store}>
                <GridProductCard
                    image="Test-image.jpg"
                    name="Test-NameProduct"
                    color="Pink"
                    cost={44}
                    article="123454"
                    productId={4}
                />
            </Provider>
        );
        fireEvent.click(getByTestId("actions__selected"))
    })

})

