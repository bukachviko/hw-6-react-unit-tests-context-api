import productsReducer, {
    addToCart,
    checkCurrentProductIdDeleteFromCart,
    checkCurrentProductIdToCart,
    clearCartFromLocalStorage,
    clearProductToCard,
    deleteFromCart,
    fetchCardData,
    initCart,
    toggleSelectedProducts
} from "../Components/Slices/productsSlice";

const initialState = {
    data: [],
    status: 'idle',
    error: null,
    currentProductIdToCart: 0,
    currentProductIdDeleteCart: 0,
    cart: [],
    selected: [],
}


describe('productsReducer', () => {

    it('should handle initCart', () => {
        const state = productsReducer(initialState, initCart());
        expect(state.cart).toEqual([]);
    });

    it('should handle checkCurrentProductIdToCart', () => {
        const state = productsReducer(initialState, checkCurrentProductIdToCart(42));
        expect(state.currentProductIdToCart).toEqual(42)
    });

    it('should handle addCart', () => {
         const actionPayload = { id: 1, name: 'Product 1', price: 20 } ;
         const state = productsReducer(initialState, addToCart(actionPayload));
         expect(state.cart).toEqual([actionPayload]);
    });

    it('should handle checkCurrentProductIdDeleteFromCart', () => {
        const state = productsReducer(initialState, checkCurrentProductIdDeleteFromCart(42));
        expect(state.currentProductIdDeleteCart).toEqual(42);
    });

    it('should handle deleteFromCart', () => {
        const initialStateWithCart = { ...initialState, cart: [{ id: 1, name: 'Product 1', price: 20 }] };
        const state = productsReducer(initialStateWithCart, deleteFromCart(1));
        expect(state.cart).toEqual([]);
    });

    it('should handle toggleSelectedProducts', () => {
        const initialStateWithSelected = { ...initialState, selected: [{ id: 1, name: 'Product 1', price: 20 }] };
        const state = productsReducer(initialStateWithSelected, toggleSelectedProducts({ id: 2, name: 'Product 2', price: 30 }));
        expect(state.selected).toEqual([
            { id: 1, name: 'Product 1', price: 20 },
            { id: 2, name: 'Product 2', price: 30 },
        ]);
    });

    it('should handle clearCartFromLocalStorage', () => {
        const initialStateWithCart = { ...initialState, cart: [{ id: 1, name: 'Product 1', price: 20 }] };
        const state = productsReducer(initialStateWithCart, clearCartFromLocalStorage());
        expect(state.cart).toEqual([]);
    });

    it('should handle clearProductToCard', () => {
        const initialStateWithCart = { ...initialState, cart: [{ id: 1, name: 'Product 1', price: 20 }] };
        const state = productsReducer(initialStateWithCart, clearProductToCard());
        expect(state.cart).toEqual([]);
    });

    it('should handle fetchCardData.pending', () => {
        const state = productsReducer(initialState, fetchCardData.pending());
        expect(state.status).toEqual('loading');
    });

    it('should handle fetchCardData.fulfilled', () => {
        const actionPayload = [{ id: 1, name: 'Product 1', price: 20 }];
        const state = productsReducer(initialState, fetchCardData.fulfilled(actionPayload));
        expect(state.status).toEqual('succeeded');
        expect(state.data).toEqual(actionPayload);
        expect(state.error).toBeNull();
    });

    it('should handle fetchCardData.rejected', () => {
        const error = 'Error fetching data';
        const state = productsReducer(initialState, fetchCardData.rejected(error));
        expect(state.status).toEqual('failed');
        expect(state.error).toEqual(error);
    });
})




