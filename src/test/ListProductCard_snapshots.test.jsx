import React from 'react';
import renderer from 'react-test-renderer';
import {Provider} from 'react-redux';
import store from "../store/store";
import ListProductCard from "../Components/ProductCard/ListProductCard";

describe('ListProductCard Component', () => {
    test('renders correctly with test data', () => {
        const tree = renderer
            .create(
                <Provider store={store}>
                    <ListProductCard
                        image="Test-image.jpg"
                        name="Test-NameProduct"
                        color="pink"
                        cost={5}
                        article="55555"
                        productId={5}
                    />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders correctly with no image', () => {
        const tree = renderer
            .create(
                <Provider store={store}>
                    <ListProductCard
                    name="Test-NoImage.jpg"
                    color="green"
                    cost={15}
                    productId={2}
                    />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('renders correctly with selected product', () => {
        const tree = renderer
            .create(
                <Provider store={store}>
                    <ListProductCard
                        image="Selected-image.jpg"
                        name="Selected-NameProduct"
                        cost={25}
                        productId={3}
                    />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    })

})