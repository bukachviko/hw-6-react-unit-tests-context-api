import React from 'react';
import {fireEvent, render} from "@testing-library/react";
import Button from "../Components/Button/Button";
import '@testing-library/jest-dom';

describe('Button component', () => {
test ("Checking the display of button text", () => {
    const btnText = 'Click me';
    const { getByText } = render(<Button text={btnText} />);
    const btnEl = getByText(btnText);
    expect(btnEl).toBeInTheDocument();
});

test('Checking the class ob the button', () => {
    const btnClass = 'btn-class';
    const { container } = render(<Button btnClass={btnClass} />);
    const btnElem = container.querySelector('button');
    expect(btnElem).toHaveClass(btnClass)
});

test('Checking onClick', () => {
    const onClickEmulation  = jest.fn();
    const { getByText } = render(<Button  text="Click me" myClick={onClickEmulation} />);
    const btnElem = getByText('Click me');
    fireEvent.click(btnElem);
    expect(onClickEmulation).toHaveBeenCalledTimes(1)
})
})

